
namespace ReviewService {

    /**
     * ReviewData is a way to validate a skill that an user has used in his work.
     */
    interface IReviewData {
        userId: string // user receiving Review
        authorId: string // user giving Review
        skillId: string // Review related skill
        comment: string // Review's comment
    }

    export class ReviewAPIService {
        ReviewCreated: IReviewData[];

        constructor() {
            this.ReviewCreated = []
        }

        createReview(ReviewData: IReviewData) {
            if (Math.random() <= 0.1) {
                return Promise.reject(new Error('Bad Gateway'))
            } else {
                this.ReviewCreated.push(ReviewData)
                return Promise.resolve(ReviewData)
            }
        }
    }

}

namespace AssessmentService {

    interface IAssessmentRowData {
        evaluator: string // user who evaluated the skill
        skill: string
        comment: string
    }

    /**
     * AssessmentData represents all the assessment (evaluations) made by colleagues for a particular user
     */
    export interface IAssessmentData {
        evaluatedUser: string
        evaluations: IAssessmentRowData[]
    }
}

function generateData(dataCount: number = 10000) {
    let inputData: AssessmentService.IAssessmentData[] = []
    const NB_EVALUATED_USERS = 5 // limit to 5 evaluated users
    for (let j = 0; j < NB_EVALUATED_USERS; j++) {
        let evaluatedUser = 'U' + j
        let evaluations = []
        for (let i = 0; i < dataCount / NB_EVALUATED_USERS; i++) {
            evaluations.push({
                evaluator: 'U1' + i,
                skill: 'SKI' + i,
                comment: 'comment ' + i
            })
        }
        // an inputData contains the evaluatedUser and a list of evaluations
        // to create Review from evaluations : evaluatedUser <=> Review.user, evaluations[*].evaluator <=> Review.author, evaluations[*].skill <=> Review.skill, evaluations[*].comment <=> validaton.comment
        inputData.push({
            evaluatedUser,
            evaluations
        })
    }
    return inputData
}

function execBulkReviewCreator() {
    let inputData = generateData()
    const reviewService = new ReviewService.ReviewAPIService()
    for (let input of inputData) {
        // TODO: create Review Data from Assessment Data
        // TODO: secure this API call to create Review
        // reviewService.createReview(convertedInput)
    }
}

execBulkReviewCreator()