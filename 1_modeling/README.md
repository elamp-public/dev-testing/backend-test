# Modeling

## Introduction


Try to write a simple model for the following use cases :

*A Task Scheduler is in charge of periodically scheduling some Tasks to a pool of Workers.*

For the scope of this test, we just want to have a model that works with the following requirements :
- Only one Task Scheduler that pushes Task to a Repository every 10 seconds
- A pool of 3 Workers that pull Task from the Repository
- A Task is composed of an ID and a Date
- Worker only console logs the Task
- Workers pull one Task at a time every 2 seconds
- **BONUS** : 2 Workers cannot pull the same Task (ie task-lock)


## Scope

NodeJS/TypeScript is preferred but you are free to use the language of your choice.
You can use any library or package that is able to perform these actions at scale.