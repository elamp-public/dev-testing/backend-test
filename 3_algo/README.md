# Algorithmic optimization

## Introduction

Some timings are set in the code (that you can see while running the code with `npm start`). The goal is to try to optimize the code specified in `index.ts` to reduce time taken to do the overall process.

## Goals

Try to optimize execution of the code specified in index :
- Highlight what is taking longer
- Find and try solutions to fix it

## Scope

- Try to keep coding in NodeJS. If another language is needed (and arguments for this are provided) try implementation in another language.
- Since it's purely algorithmic, it's primilarly not necessary to rely on another packages than those provided by NodeJS core.
